import pytest
from utils.cnpj_validator_util import CnpjValidator

valid_cnpjs = [
    '58577114000189',
    '11222333000181',
    '00038174000143',  # UnB
]

invalid_cnpjs = [
    '11111111111111',
]

invalid_null_cnpjs = [
    '00000000000000',
]

wrong_length_or_non_numeric_cnpjs = [
    # CNPJs com comprimento errado
    '',
    '1234567890123',
    '123456789012345',
    # CNPJs com caracteres não numéricos
    '0003817400014_',
    'F0038174000143',
    '00!38174000143',
]

sut = CnpjValidator.validate

@pytest.mark.parametrize('cnpj', valid_cnpjs)
def test_accepts_valid_cnpj(cnpj: str):
    try:
        sut(cnpj)
    except Exception as e:
        pytest.fail(f"Expected no exception for valid CNPJ, but got {e}")

@pytest.mark.parametrize('cnpj', wrong_length_or_non_numeric_cnpjs)
def test_rejects_cnpj_with_non_numeric_digits_or_wrong_length(cnpj: str):
    with pytest.raises(Exception) as e:
        sut(cnpj)
    assert 'CNPJ must contain exactly 14 numerical digits' in str(e.value)

@pytest.mark.parametrize('cnpj', invalid_cnpjs + invalid_null_cnpjs)
def test_rejects_invalid_cnpjs(cnpj: str):
    with pytest.raises(Exception) as e:
        sut(cnpj)
    assert 'Invalid' in str(e.value)
