import pytest
from unittest.mock import patch, MagicMock
from tariffs.models import Distributor

@pytest.mark.django_db
def test_delete_tariffs_by_subgroups_exception():
    distributor_instance = Distributor()
    distributor_instance.id = 1
    request_subgroup = 'some_subgroup'

    with patch('tariffs.models.Tariff.objects.get') as mock_get:
        mock_get.side_effect = [MagicMock(), MagicMock()]

        result = distributor_instance.delete_tariffs_by_subgroups(request_subgroup)

    assert result == 'Tariffs by subgroups deleted successfully'
